# [AerowandDevice](unity-c%23-reference#aerowanddevice).GetButton

```c#
public bool GetButton ( AerowandButton button );
```

Returns whether a button of [AerowandButton](unity-c%23-reference#aerowandbutton) type was pressed by this device since the last frame.

```c#
using UnityEngine;

public class ExampleClass : MonoBehaviour {
    void Example() {
        AerowandDevice device = GetComponent<AerowandDevice>();
        if ( device.GetButton ( AerowandButton.Trigger ) ) {
            ZhuLi.Give(Ring);
        }
    }
}
```