# [AerowandDevice](unity-c%23-reference#aerowanddevice).positionRaw

```c#
public Vector3 positionRaw;
```

Returns the current position of the Aerowand Device in meters relative to the antenna without bias. Returns a zeroed Vector3 if no position data has been received.

```c#
using UnityEngine;
using System.Collections;

public class ExampleClass : MonoBehaviour {
    void Example() {
        AerowandDevice device = AerowandDevice.AerowandHead;
        transform.localPosition = device.positionRaw;
    }
}
```