[[_TOC_]]

# Overview

The Aerowand can be quickly integrated into new and existing Unity projects by importing the Aerowand Asset Package from either the Unity Asset Store or from the download here (links forthcoming). Position and rotation tracking can be achieved with existing components provided in the Aerowand Package; no scripting necessary. This document will give outline the minimum steps needed to import the Aerowand package and integrate controls into new and existing projects.

# Prerequisites

*  Install [Unity 5.6.0 Beta](https://unity3d.com/unity/beta/#notes) or newer
*  Download Aerowand Asset Package (link forthcoming)

# Setup

The first step to integrating the Aerowand into a project is by importing the Aerowand Asset Package in the file **Aerowand.unitypackage**. Asset packages can be imported from either the Unity Asset Store or manually via the Unity window menu bar's ``Assets > Import Package > Custom Package...``. Navigate to the download location for Aerowand.unitypackage, select it, and open.

A new window titled **Import Unity Package** should open. Click import in the bottom right of the window to import all Aerowand Package assets. When done loading the Aerowand Package will be added to the project.

![Importing Aerowand Package](videos/unitypackage.mp4)

Next we need to add VR support to the Unity project if it was not previously enabled. Open the **PlayerSettings** in Unity's Inspector Window via the Unity window menu bar's ``Edit > Project Settings > Player``. In the Inspector navigate to ``Other Settings > Virtual Reality Supported`` checkbox and check it.

Under the Virtual Reality Supported checkbox is a **Virtual Reality SDKs** menu. Click the + in the bottom right of the Virtual Reality SDKs menu and select all Virtual Reality displays you want to build for[^1] - e.g. Daydream, Cardboard, Oculus, etc.

![Configuring Unity for Aerowand](videos/unity config.mp4)

[^1]: Available virtual reality displays will vary by platform, if your desired platform is missing make sure that the Unity project is set to build to the correct OS platform under the Unity window menu ``File > Build Settings...``

# Adding Aerowand support to a new project

Adding Aerowand tracking to a new project is as simple as dragging and dropping the **Aerowand Head** and **Aerowand Hand** prefabs located in the Project Window under `Aerowand > Prefabs` directory to either the Hierarchy or Scene Windows.

Examples of how to detect button input from an Aerowand device can be found in the [GetButtonDown](AerowandDevice.GetButtonDown), [GetButtonUp](AerowandDevice.GetButtonUp), and [GetButton](AerowandDevice.GetButton) script reference pages.

![Adding Aerowand Head to Scene](videos/head prefab.mp4)

![Adding Aerowand Hand to Scene](videos/hand prefab.mp4)

# Adding Aerowand support to an existing project

Two Components need to be added to any pre-existing GameObject to have them track to an Aerowand device's position and rotation: **AerowandDevice** and **ReferencedTransform**. Both scripts can be found in the Project Window under `Aerowand > Scripts`.

Examples of how to detect button input from an Aerowand device can be found in the [GetButtonDown](AerowandDevice.GetButtonDown), [GetButtonUp](AerowandDevice.GetButtonUp), and [GetButton](AerowandDevice.GetButton) script reference pages.

When adding an AerowandDevice Component to a GameObject, change the Tracker Type to **Head** or **Pointer** to have the GameObject track to head or hand tracker respectively.

![Adding Aerowand head tracking to Main Camera](videos/add to camera.mp4)

![Adding Aerowand hand tracking to a Net GameObject](videos/add to net.mp4)