# [AerowandDevice](unity-c%23-reference#aerowanddevice).GetButtonUp

```c#
public bool GetButtonUp ( AerowandButton button );
```

Returns whether a button of [AerowandButton](unity-c%23-reference#aerowandbutton) type was pressed by this device since the last frame.

```c#
using UnityEngine;

public class ExampleClass : MonoBehaviour {
    void Example() {
        AerowandDevice device = GetComponent<AerowandDevice>();
        if ( device.GetButtonUp ( AerowandButton.Trigger ) ) {
            Bolin.Do(TheSpecificThing); // I never have to explain TheThing to ZhuLi
        }
    }
}
```