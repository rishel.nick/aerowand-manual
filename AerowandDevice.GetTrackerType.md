# [AerowandDevice](unity-c%23-reference#aerowanddevice).GetTrackerType

```c#
public AerowandTracker GetTrackerType;
```

Returns the type of [AerowandTracker](unity-c%23-reference#aerowandtracker) associated to the Aerowand device. An AerowandDevice's trackerType is settable in Unity's Inspector.

```c#
using UnityEngine;

public class ExampleClass : MonoBehaviour {
    void Example() {
        AerowandDevice device = GetComponent<AerowandDevice>();
        switch ( device.GetTrackerType ) {
            case AerowandTracker.Head:
                Debug.LogError("This device is for tacking the user's head");
                break;
            case AerowandTracker.Pointer:
                Debug.LogError("This device is for tacking the user's hand");
                break;
            default:
                Debug.LogError("Exciting new Aerowand tracker types. :D");
                break;
        }
    }
}
```