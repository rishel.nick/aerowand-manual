# [AerowandDevice](unity-c%23-reference#aerowanddevice).GetButtonDown

```c#
public bool GetButtonDown ( AerowandButton button );
```

Returns whether a button of [AerowandButton](unity-c%23-reference#aerowandbutton) type was pressed by this device since the last frame.

```c#
using UnityEngine;

public class ExampleClass : MonoBehaviour {
    void Example() {
        AerowandDevice device = GetComponent<AerowandDevice>();
        if ( device.GetButtonDown ( AerowandButton.Trigger ) ) {
            ZhuLi.Do(TheThing); // Thank you Zhu Li.
        }
    }
}
```