# [AerowandDevice](unity-c%23-reference#aerowanddevice).AerowandHead

```c#
public static AerowandDevice AerowandHead;
```

Returns an AerowandDevice identified as tracking the user's head. Null if no AerowandDevice exists in the scene with [AerowandTracker](unity-c%23-reference#aerowandtracker) type set to Head[^1].

[^1]: An AerowandDevice's trackerType is settable in Unity's Inspector.