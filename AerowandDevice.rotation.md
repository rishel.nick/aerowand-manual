# [AerowandDevice](unity-c%23-reference#aerowanddevice).rotation

```c#
public Quaternion rotation;
```

Returns the current rotation of the Aerowand Device relative to the antenna. Returns an identity quaternion if no position data has been received.

```c#
using UnityEngine;
using System.Collections;

public class ExampleClass : MonoBehaviour {
    void Example() {
        AerowandDevice device = AerowandDevice.AerowandHead;
        transform.localRotation = device.rotation;
    }
}
```