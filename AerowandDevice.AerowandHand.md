# [AerowandDevice](unity-c%23-reference#aerowanddevice).AerowandHand

```c#
public static AerowandDevice AerowandHand;
```

Returns an AerowandDevice identified as tracking the user's hand. Null if no AerowandDevice exists in the scene with [AerowandTracker](unity-c%23-reference#aerowandtracker) type set to Pointer[^1].

[^1]: An AerowandDevice's trackerType is settable in Unity's Inspector.