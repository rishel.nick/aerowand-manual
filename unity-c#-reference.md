[[_TOC_]]

# AerowandDevice

MonoBehaviour inheriting class attached to a GameObject in Unity to expose an Aerowand Tracking device information to a game.

| Static Variables | Description |
| --- | --- |
| [AerowandHead](AerowandDevice.AerowandHead) | Returns an AerowandDevice identified as tracking the user's head |
| [AerowandHand](AerowandDevice.AerowandHand) | Returns an AerowandDevice identified as tracking the Aerowand Pointer controller |

| Variables       | Description |
| --- | --- |
| [GetTrackerType](AerowandDevice.GetTrackerType) | Returns the type of AerowandTracker associated to the Aerowand device |
| [position](AerowandDevice.position) | Returns the last received position of the Aerowand device |
| [rotation](AerowandDevice.rotation) | Returns the last received rotation of the Aerowand device |
| [positionRaw](AerowandDevice.positionRaw) | Returns the last received position of the Aerowand device without bias |
| [rotationRaw](AerowandDevice.rotationRaw) | Returns the last received rotation of the Aeroand device without bias |

| Public Functions | Description |
| --- | --- |
| [GetButtonDown](AerowandDevice.GetButtonDown) | Returns whether the queried button was pressed since the last frame |
| [GetButtonUp](AerowandDevice.GetButtonUp) | Returns whether the queried button was released since the last frame |
| [GetButton](AerowandDevice.GetButton) | Returns whether the queried button is currently pressed down |

# AerowandButton

Enum for buttons available on the Aerowand Pointer device.

| Memeber | Description |
| --- | --- |
| Face | Primary button on the face of an Aerowand Pointer device where a user's thumb rests |
| Trigger | Primary trigger button under the Aerowand Pointer device |

# AerowandTracker

Enum indicating what an AerowandDevice is tracking.

| Memeber | Description |
| --- | --- |
| Head | Device is tracking the user's head |
| Pointer | Device is tracking the pointer controller, i.e. the user's hand |